var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

const crypto = require('crypto');

//para permitir cors
var cors = require('cors')
app.use(cors())

//variable para poder usar el request-json
var requestJson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // para parsear el tipo application/json

//******Para conectar a un mongodb local **********
var mongoClient = require('mongodb').MongoClient;
//para poder usar el tipo NumberInt en las consultas de mongo
var NumberInt = require('mongodb').NumberInt;
//para poder consultar el o_id
var ObjectId = require('mongodb').ObjectID;
//locashost se debería cambiar por el nombre del contenedor donde se ejecuta
// var url = "mongodb://localhost:27017/local";
var url = "mongodb://servermongo:27017/local";

//*****Para usar las librerias de Postgre********
var pg = require('pg');
//url para conectarnos al contenedor de la bbdd postgre
//cambiar el localhost por el nombre del contenedor que le hemos dado al lanzarlo cuando se ejecute desde el contenedor
// var urlUsuarios = "postgres://docker:docker@localhost:5432/bdseguridad";
var urlUsuarios = "postgres://docker:docker@serverpostgre:5432/bdseguridad";
var clientePostgre = new pg.Client(urlUsuarios);

//******Mlab******
var urlMovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?";
//revisar si esto tiene que estar aquí
var apiKeyMlab = "apiKey=ECsd8XfvywLHm3Hl6qOOV9pF7yjp4ejM"
//var clienteMlab = requestJson.createClient(urlmovimientosMlab);

app.listen(port);

console.log('Arrancado RestFul API server con Node en puerto: ' + port);

//Servicios

function sesionMlab(nif, tk)
{
/* Metemos el token en MLab */
  var clienteMlab = requestJson.createClient(urlMovimientosMlab + apiKeyMlab);
  var reqBody = {"nif": nif,"token": tk};
  clienteMlab.post('', reqBody, function(err, resM, body) {
    if(err)
    {
      console.log(err + " " + body);
    }
  });
}

app.post('/login', function(req, res) {
  //Crear cliente PostgreSQL
  clientePostgre.connect();
  //Hacer consulta
  const query = clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE nif=$1 AND password=$2;',
       [req.body.login, req.body.password], (err, result) => {
    if (err) {
      res.send(err);
    }
    else {
      if (result.rows[0].count >= 1)
      {
        const query2 = clientePostgre.query('SELECT * FROM usuarios WHERE nif=$1 AND password=$2;',
              [req.body.login, req.body.password], (err, result) => {
          if (err) {
            res.send(err);
          }
          else {
            if (result.rows[0].id > 0)
            {
              var token = crypto.randomBytes(64).toString('hex');
              //genero la sesion en Mlab
              sesionMlab(result.rows[0].nif, token);
              res.send({mensaje:"Login correcto",salida:result.rows[0],tk:token});
            }
            else {
              res.send({mensaje:"Login INcorrecto",salida:result.rows[0]});
            }
          }
        });      
      }
      else {
        res.send({mensaje:"Login INcorrecto",salida:result.rows[0]});
      }
    }
  });
});

app.put('/cliente', function(req, res) {
  //Crear cliente PostgreSQL
  clientePostgre.connect();
  const query = clientePostgre.query('UPDATE usuarios SET nif=($2), password=($3), nombre=($4), apellido=($5), direccion=($6), cp=($7) WHERE id=($1);',
      [req.body.id, req.body.nif, req.body.password, req.body.nombre, req.body.apellido, req.body.direccion, req.body.cp], (err, result) => {
     if (err) {
      res.send(err);
    }
    else {
      res.send({mensaje:"modificación correcta",salida:req.body});
    }
  });
});

app.post('/cliente', function(req, res) {
  //Crear cliente PostgreSQL
  clientePostgre.connect();
  const query = clientePostgre.query('INSERT INTO usuarios(nif, password, nombre, apellido, direccion, cp) values($1, $2, $3, $4, $5, $6);',
       [req.body.nif, req.body.password, req.body.nombre, req.body.apellido, req.body.direccion, req.body.cp], (err, result) => {
     if (err) {
      res.send(err);
    }
    else 
    {
      const query2 = clientePostgre.query('SELECT * FROM usuarios WHERE nif=$1 AND password=$2;',
              [req.body.nif, req.body.password], (err, result) => {
          if (err) {
            res.send(err);
          }
          else {
            if (result.rows[0].id > 0)
            {
              res.send({mensaje:"Alta correcta",salida:result.rows[0]});
            }
            else {
              res.send({mensaje:"Login INcorrecto",salida:result.rows[0]});
            }
          }
        });
    }
  });
});

app.get('/cuentas10/:id', function(req, res) {
    mongoClient.connect(url, function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        var col = db.collection('Cuentas');
        var q = {"dni": req.params.id};
        col.find(q).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    })
});

app.get('/movimientos10/:id', function(req, res) {
    mongoClient.connect(url, function(err, db) {
      if (err)
      {
        console.log(err);
      }
      else {
        var col = db.collection('Movimientos');
        var q = {idCuenta: Number(req.params.id)};
        col.find(q).sort({"fechaMovimiento": 1, "horaMovimiento": 1}).toArray(function(err, docs) {
          res.send(docs);
        });
        db.close();
      }
    })
});

app.post('/cuenta3', function(req, res) {
    mongoClient.connect(url, function(err, db) {
      var col = db.collection('Cuentas');
      col.insert(req.body, function(err, r) {
        console.log(r.insertedCount + ' registros insertados body');
      });

        db.close();
        res.send("ok");
    })
});

app.put('/cuenta3', function(req, res) {
    mongoClient.connect(url, function(err, db) {
      var col = db.collection('Cuentas');
      var o_id = new ObjectId(req.body.id);
      var s={_id:o_id};
      var m={$set: {movimientos:req.body.movimientos, saldo:req.body.saldo}};
      col.updateOne(s, m, function(err, r) {
        if (err) throw err;
        console.log(r.result.nModified + ' registro actualizado body');
      });

        db.close();
        res.send("ok el put");
    })
});


app.post('/mov3', function(req, res) {
    mongoClient.connect(url, function(err, db) {
      var col = db.collection('Movimientos');
      col.insert(req.body, function(err, r) {
        console.log(r.insertedCount + ' registros insertados body');
      });

        db.close();
        res.send("ok");
    })
});
